/* eslint no-param-reassign: ["error", { "props": false }] */
/* eslint react/forbid-prop-types: off */

import React from 'react';
import PropTypes from 'prop-types';
import GalleryEditor from './GalleryEditor';

export default function AddDataModal(props) {
  AddDataModal.propTypes = {
    availableGalleryImageDataObjects: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  return (
    <div
      id='addDataModal'
      className="modal fade"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLiveLabel"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h3 className="modal-title" id="exampleModalLiveLabel">Add new data</h3>
          </div>
          <div className="modal-body">
            <h4>Enter the details</h4>

            <form id="newDataForm">
              <div className="form-group" id="title">
                <label htmlFor="Title" className="Form-label">Title:</label>
                <input
                  className="form-control Data"
                  type="text"
                  id='titleData'
                  name="Title"
                  placeholder="title"
                />
              </div>
            </form>

            <div id='gallery'>
              <GalleryEditor
                availableGalleryImageDataObjects={props.availableGalleryImageDataObjects}
              />
            </div>
          </div>
          <div className="modal-footer" id='continueFooter'>
            <button
              type="button"
              className="btn btn-primary Trails-button"
              data-dismiss="modal"
            >
              Cancel
            </button>
        </div>
      </div>
    </div>
  </div>
  );
}
