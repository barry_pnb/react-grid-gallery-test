import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import AddDataModal from './AddDataModal';

class App extends Component {
  constructor(props) {
    super(props);
    this.triggerNewImageData = this.triggerNewImageData.bind(this);
    this.state = { available_gallery_images: [ {
      url: 'http://www.cameraegg.org/wp-content/uploads/2015/06/canon-powershot-g3-x-sample-images-1.jpg',
    },
    {
      url: 'https://kbob.github.io/images/sample-4.jpg',
    }] };
  }

  triggerNewImageData() {
    this.setState({ available_gallery_images:
      [ ...this.state.available_gallery_images,  {url: 'https://orig00.deviantart.net/579b/f/2015/070/1/f/profile_picture_by_angrybirdsstuff-d8l9xzo.jpg'}]
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div style={{margin: 20}}>
        <button
          type="button"
          className="btn btn-primary"
          data-toggle="modal"
          data-target='#addDataModal'
        >
          Create gallery
        </button>
        </div>
        <div>
        <button
          type="button"
          className="btn btn-primary"
          onClick={this.triggerNewImageData}
        >
          Trigger new image data
        </button>
        </div>
        <AddDataModal
          availableGalleryImageDataObjects={this.state.available_gallery_images.map(a => ({ ...a }))
          }
        />
      </div>
    );
  }
}

export default App;
