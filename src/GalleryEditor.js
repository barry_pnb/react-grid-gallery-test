/* eslint react/forbid-prop-types: off */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GalleryGrid from 'react-grid-gallery';

export default class GalleryEditor extends Component {
  constructor(props) {
    super(props);
    GalleryEditor.propTypes = {
      availableGalleryImageDataObjects: PropTypes.arrayOf(PropTypes.object).isRequired
    };
    this.onSelectImage = this.onSelectImage.bind(this);
    this.processAvailableGalleryImageDaytaObjects
      = this.processAvailableGalleryImageDataObjects.bind(this);
    this.state = {
      imageDataObjects: [],
    };
  }

  componentWillMount() {
    this.processAvailableGalleryImageDataObjects(this.props.availableGalleryImageDataObjects);
  }

  componentWillReceiveProps(newProps) {
    this.processAvailableGalleryImageDataObjects(newProps.availableGalleryImageDataObjects);
  }

  onSelectImage(index /* , image */) {
    const images = this.galleryGrid.state.images.slice();
    const img = images[index];
    if (Object.prototype.hasOwnProperty.call(img, 'isSelected')) {
      img.isSelected = !img.isSelected;
    } else {
      img.isSelected = true;
    }
    this.props.handleSelection(img);

    this.galleryGrid.setState({ images });
  }

  processAvailableGalleryImageDataObjects(availableGalleryImageDataObjects) {
    this.setState({imageDataObjects: []});
    availableGalleryImageDataObjects.forEach((imageTracker) => {
      // not great. As we don't know the dimensions of the image
      // we load it into an Image object to determine them
      const newImg = new Image();
      newImg.onload = () => {
        const height = newImg.height;
        const width = newImg.width;
        const currentImageDataObjects = [];
        const newImageData = {
          src: imageTracker.url,
          thumbnail: imageTracker.url,
          thumbnailWidth: width,
          thumbnailHeight: height,
        };
        currentImageDataObjects.push(newImageData);
        this.setState({ imageDataObjects: [ ...this.state.imageDataObjects, ...currentImageDataObjects]});
      };
      // this triggers the onload
      newImg.src = imageTracker.url;
    });
  }

  render() {
    return (
      <div>
        <div style={{ display: 'flex' }}>
          <label className="Form-label" htmlFor="galleryEditor">Gallery:</label>
        </div>
        <div className="row">
          <GalleryGrid
            images={this.state.imageDataObjects}
            enableImageSelection
            id='galleryGrid'
            onSelectImage={this.onSelectImage}
          />
        </div>
      </div>
    );
  }
}
